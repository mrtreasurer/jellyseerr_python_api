import requests

from urllib.parse import quote_plus

from .exception import JellyseerrException
from .models import User


class JellyseerrBase:
    def __init__(self, url: str, api_key: str=None) -> None:
        """This class implements basic functionality and manages authentication to the Jellyseerr API.

        :param url: Jellyseerr api url. Should end with /api/v1.
        :type url: str.
        :param api_key: Jellyseerr API Key, defaults to None. If not provided, it can be set later or obtained by signing in with username and password.
        :type api_key: str, optional
        """
        self.url = url

        self._api_key = None
        if api_key is not None:
            self.api_key = api_key

    @property
    def api_key(self) -> str:
        return self._api_key

    @api_key.setter
    def api_key(self, api_key:str):
        """_summary_

        :param api_key: _description_
        :type api_key: _type_
        """

        # provisionally set new api key
        current_key = self._api_key
        self._api_key = api_key

        # test successful request with api key
        try:
            _ = self.auth_me()

        except JellyseerrException:
            # if unauthorised, raise error and restore key
            self._api_key = current_key

            raise

    def _request(self, method, *args, **kwargs) -> requests.Response:
        """Make request to Jellyseerr server. `args` and `kwargs` are passed to \
            the appropriate method of the `requests` module. \

        :param method: Request method to be used: get, post, put, etc.
        :type method: str
        """

        # percent encode params
        if "params" in kwargs:
            kwargs["params"] = {key: quote_plus(str(value).encode("utf8")) for key, value in kwargs["params"].items()}


        if (self._api_key is not None) and ("cookies" not in kwargs):
            kwargs["headers"] = {"X-Api-Key": self._api_key}

        try:
            response = requests.request(method, *args, **kwargs)

        except requests.Timeout:
            raise JellyseerrException("Connection timeout. Check connection.")

        except requests.ConnectionError:
            raise JellyseerrException("Connection error. Check configuration.")

        else:
            return response

    def _get(self, *args, **kwargs):
        """Send get request. Calls _request method"""
        return self._request("get", *args, **kwargs)

    def _post(self, *args, **kwargs):
        """Send post request. Calls _request method"""
        return self._request("post", *args, **kwargs)

    def _delete(self, *args, **kwargs):
        return self._request("delete", *args, **kwargs)

    def auth_me(self) -> User:
        """Call to auth/me endpoint. Returns active user."""
        try:
            response = self._get(self.url + "auth/me")
            response.raise_for_status()

        except requests.HTTPError as e:
            raise JellyseerrException(e.response.json())

        else:
            content = response.json()
            return User.deserialize(content)

    def auth_jellyfin(self, username:str, password:str, email:str=None, hostname:str=None) -> User:
        """Log in using Jellyfin account. Will first log in using username and password and email if it is given. \
        If that does not succeed with "No hostname provided error", will retry with hostname if it is provided.

        :param username: Username of Jellyfin account.
        :type username: str
        :param password: Password of Jellyfin account.
        :type password: str
        :param email: Email address of Jellyfin account, defaults to None.
        :type email: str, optional
        :param hostname: Url of the Jellyfin server holding the account, defaults to None. Required if server \
        is not set.
        :type hostname: str, optional
        """

        def _login(req_payload):
            """Log in using provided credentials and return response if successfull. \
                Raise JellyseerrException if not."""
            response = self._post(self.url + "auth/jellyfin", json=req_payload)
            response.raise_for_status()
            content = response.json()
            return User.deserialize(content), response.cookies

        payload = {
            "username": username,
            "password": password
        }

        if email:
            payload.update({"email": email})

        try:
            user, cookie_jar = _login(payload)

        except requests.HTTPError as e:
            if (e.response.json() == {"error": "No hostname provided."}) and (hostname is not None):
                payload.update({"hostname": hostname})

                user, cookie_jar = _login(payload)

            else:
                raise JellyseerrException(e.response.json())

        response = self._get(self.url + "settings/main", cookies=cookie_jar)
        content = response.json()
        self.api_key = content["apiKey"]
        return user

