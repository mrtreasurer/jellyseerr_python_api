from .base_model import BaseModel


class MediaInfo(BaseModel):
    __fields__ = (
        "id",
        "tmdbId",
        "tvdbId",
        "status",
        "createdAt",
        "updatedAt",
    )

    def __init__(
            self,
            id:str,
            tmdb_id:str,
            tvdb_id:str,
            status:str,
            created_at:str,
            updated_at:str
            ) -> None:

        super().__init__()

        self.id = id
        self.tmdb_id = tmdb_id
        self.tvdb_id = tvdb_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
