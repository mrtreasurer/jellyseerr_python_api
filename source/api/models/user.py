from .base_model import BaseModel


class User(BaseModel):
    __fields__ = (
        "id",
        "email",
        "username",
        "jellyfinUsername",
        "jellyfinAuthToken",
        "userType",
        "permissions"
    )

    def __init__(
            self,
            id:int,
            email:str,
            username:str,
            jellyfin_username:str,
            jellyfin_auth_token:str,
            user_type:str,
            permissions:int
            ) -> None:

        super().__init__()

        self.id = id
        self.email = email
        self.username = username
        self.jellyfin_username = jellyfin_username
        self.jellyfin_auth_token = jellyfin_auth_token
        self.user_type = user_type
        self.permissions = permissions
