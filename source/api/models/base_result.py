from datetime import datetime

from .base_model import BaseModel


class BaseResult(BaseModel):
    """Base class for TvResult, MovieResult. Defines a few properties"""
    DATE_FIELD = "date"
    NAME_FIELD =  "name"
    POSTER_FIELD = "poster_path"

    missing_poster = "https://artworks.thetvdb.com/banners/images/missing/movie.jpg"

    @property
    def media_date(self) -> str:
        """return variable associated with DATE class variable"""
        return getattr(self, self.DATE_FIELD)

    @property
    def media_name(self) -> str:
        """return variable associated with NAME class variable"""
        return getattr(self, self.NAME_FIELD)

    @property
    def year(self) -> int|None:
        """Parse date string using datetime and return year. If parsing fails, return None."""
        try:
            return datetime.strptime(self.media_date, "%Y-%m-%d").year

        except (ValueError, TypeError):
            return None

    def get_poster_path(self) -> str:
        """Return full poster path from tmdb. If no image path is given, return link for missing image."""
        if getattr(self, self.POSTER_FIELD) is not None:
            return f"https://image.tmdb.org/t/p/w500{getattr(self, self.POSTER_FIELD)}"

        else:
            return self.missing_poster

    def __str__(self):
        s = f"{self.media_name} ({self.year})"
        if hasattr(self, "overview"):
            s += f"\n\n{self.overview}"

        return s
