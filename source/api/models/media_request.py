from datetime import datetime, timezone
from dateutil import tz

from .base_model import BaseModel
from .media_info import MediaInfo
from .user import User

class MediaRequest(BaseModel):
    """Response of a request to the /request endpoint
    MediaRequest: {
        id: int
        status: int
        media: MediaInfo
        createdAt: string
        updatedAt: string
        requestedBy: User
        modifiedBy: User
        is4k: bool
        serverId: int
        profileId: int
        rootFolder: str
    }
    """
    __fields__ = (
        "id",
        "type",
        "status",
        "media",
        "createdAt",
        "updatedAt",
        "requestedBy",
        "modifiedBy",
    )

    __model_fields__ = {
        "media": MediaInfo,
        "requestedBy": User,
        "modifiedBy": User,
    }

    def __init__(
            self,
            id:int,
            type:str,
            status:int,
            media:int,
            created_at:str,
            updated_at:str,
            requested_by:User,
            modified_by:User,
            ) -> None:

        super().__init__()

        self.id = id
        self.type = type
        self.status = status
        self.media = media
        self.created_at = created_at
        self.updated_at = updated_at
        self.requested_by = requested_by
        self.modified_by = modified_by

    def _convert_time(self, dt) -> datetime:
        return datetime.strptime(dt[:-5], "%Y-%m-%dT%H:%M:%S").replace(tzinfo=timezone.utc).astimezone(tz.tzlocal())

    @property
    def creation_date(self) -> datetime:
        return self._convert_time(self.created_at)

    @property
    def update_date(self) -> datetime:
        return self._convert_time(self.updated_at)
