from .base_result import BaseResult


class MovieResult(BaseResult):
    __fields__ = (
        "id",
        "posterPath",
        "overview",
        "releaseDate",
        "title",
        "mediaType"
    )

    DATE_FIELD = "release_date"
    NAME_FIELD = "title"

    def __init__(
            self,
            id:int,
            poster_path:str,
            overview:str,
            release_date:str,
            title:str,
            media_type:str
            ) -> None:

        super().__init__()

        self.id = id
        self.poster_path = poster_path
        self.overview = overview
        self.release_date = release_date
        self.title = title
        self.media_type = media_type
