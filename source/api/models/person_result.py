from .base_result import BaseResult


class PersonResult(BaseResult):
    __fields__ = (
        "id",
        "name",
        "mediaType",
        "profilePath"
    )

    POSTER_FIELD = "profile_path"

    missing_poster = "https://artworks.thetvdb.com/banners/images/missing/person.jpg"

    def __init__(
            self,
            id:int,
            name:str,
            media_type:str,
            profile_path:str
            ) -> None:

        super().__init__()

        self.id = id
        self.name = name
        self.media_type = media_type
        self.profile_path = profile_path

    def media_date(self) -> None:
        return None

    def year(self) -> None:
        return None

    def __str__(self):
        return self.media_name
