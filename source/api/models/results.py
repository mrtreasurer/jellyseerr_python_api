from .base_model import BaseModel

from .base_result import BaseResult
from .media_factory import MediaItemFactory


class Results(BaseModel):
    """Result of a get request to /search"""
    __fields__ = (
        "page",
        "totalPages",
        "totalResults",
        "results"
    )
    __model_fields__ = {
        "results": MediaItemFactory
    }

    def __init__(
            self,
            page:int,
            total_pages:int,
            total_results:int,
            results: BaseResult
            ) -> None:

        super().__init__()

        self.page = page
        self.total_pages = total_pages
        self.total_results = total_results
        self.results = results

    def __getitem__(self, i) -> BaseResult:
        return self.results[i]
