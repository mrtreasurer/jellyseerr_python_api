from .base_model import BaseModel
from .media_request import MediaRequest


class PageInfo(BaseModel):
    __fields__ = (
        "pages",
        "pageSize",
        "results",
        "page"
    )

    def __init__(
            self,
            pages:int,
            page_size:int,
            results:int,
            page:int
            ) -> None:

        super().__init__()

        self.pages = pages
        self.page_size = page_size
        self.results = results
        self.page = page


class RequestList(BaseModel):
    """Result of a get request to /requests. lists all current requests
    From source code, structure is:

    """
    __fields__ = (
        "pageInfo",
        "results"
    )

    __model_fields__ = {
        "pageInfo": PageInfo,
        "results": MediaRequest
    }

    def __init__(
            self,
            page_info: PageInfo,
            results: list[MediaRequest]
            ) -> None:

        super().__init__()

        self.page_info = page_info
        self.results = results

    def __getitem__(self, i) -> MediaRequest:
        return self.results[i]
