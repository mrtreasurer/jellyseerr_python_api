from .base_result import BaseResult


class Episode(BaseResult):
    __fields__ = (
        "id",
        "name",
        "episodeNumber",
        "overview",
        "seasonNumber",
        "showId"
    )

    def __init__(
            self,
            id:int,
            name:str,
            episode_number:int,
            overview:int,
            season_number:int,
            show_id:int
            ) -> None:

        super().__init__()

        self.id = id
        self.name = name
        self.episode_number = episode_number
        self.overview = overview
        self.season_number = season_number
        self.show_id = show_id


class Season(BaseResult):
    __fields__ = (
        "id",
        "name",
        "overview",
        "posterPath",
        "seasonNumber",
        "episodes"
    )

    __model_fields__ = {
        "episodes": Episode
    }

    def __init__(
            self,
            id:int,
            name:int,
            overview:int,
            poster_path:int,
            season_number:int,
            episodes:Episode
            ) -> None:

        super().__init__()

        self.id = id
        self.name = name
        self.overview = overview
        self._poster_path = poster_path
        self.season_number = season_number
        self.episodes = episodes