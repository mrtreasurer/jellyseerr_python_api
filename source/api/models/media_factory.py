from .base_model import BaseModel
from .movie_result import MovieResult
from .tv_result import TvResult
from .person_result import PersonResult


class MediaItemFactory(BaseModel):
    @classmethod
    def deserialize(cls, json_data:dict):
        if json_data["mediaType"] == "movie":
            return MovieResult.deserialize(json_data)

        elif json_data["mediaType"] == "tv":
            return TvResult.deserialize(json_data)

        elif json_data["mediaType"] == "person":
            return PersonResult.deserialize(json_data)

        raise ValueError("Invalid mediaType provided")
