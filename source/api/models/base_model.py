from inflection import underscore


class BaseModel:
    """Base class for all models.
    __fields__ is an iterable of all the fields defined in the class. This is used to generate the instances from the json
    __model_fields__ is a mapping of the fields that represent other objects and the class object that should be used.
    """
    __fields__: tuple[str, ...] = tuple()
    __model_fields__: dict[str, object] = dict()

    @classmethod
    def deserialize(cls, json_data:dict):
        model_data = {underscore(camel_key): json_data.get(camel_key, None) for camel_key in cls.__fields__}

        for camel_key, model in cls.__model_fields__.items():
            key = underscore(camel_key)
            if isinstance(model_data[key], list):
                model_data[key] = [model.deserialize(model_i) for model_i in model_data[key]]

            elif model_data[key] is not None:
                model_data[key] = model.deserialize(model_data[key])

        return cls(**model_data)
