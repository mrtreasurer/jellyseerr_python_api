from .base_result import BaseResult


class TvResult(BaseResult):
    __fields__ = (
        "id",
        "posterPath",
        "overview",
        "firstAirDate",
        "name",
        "mediaType"
    )

    DATE_FIELD = "first_air_date"
    NAME_FIELD = "name"

    def __init__(
            self,
            id:int,
            poster_path:str,
            overview:str,
            first_air_date:str,
            name:str,
            media_type:str
            ) -> None:

        super().__init__()

        self.id = id
        self.poster_path = poster_path
        self.overview = overview
        self.first_air_date = first_air_date
        self.name = name
        self.media_type = media_type
