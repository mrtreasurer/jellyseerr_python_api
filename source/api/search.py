from .base import JellyseerrBase
from .models import Results


class JellyseerrSearch(JellyseerrBase):
    def search(self, query: str, page: int=None, language: str=None) -> Results:
        """GET request on search endpoint

        :param query: Search query/
        :type query: str
        :param page: Which page to return, defaults to 1
        :type page: int, optional
        :param language: Language of results, defaults to "en"
        :type language: str, optional
        :return: Result object
        :rtype: source.api.models.result.Result
        """
        payload = {
            "query": query
        }

        if page is not None:
            payload.update({"page": page})

        if language is not None:
            payload.update({"language": language})


        response = self._get(self.url + "search", params=payload)
        return Results.deserialize(response.json())
