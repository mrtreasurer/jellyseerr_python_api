from .base import JellyseerrBase
from .search import JellyseerrSearch
from .request import JellyseerrRequest


class JellyseerrAPI(JellyseerrRequest, JellyseerrSearch, JellyseerrBase):
    pass