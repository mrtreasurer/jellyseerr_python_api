import requests

from .base import JellyseerrBase
from .exception import JellyseerrException
from .models import MediaRequest, RequestList


class JellyseerrRequest(JellyseerrBase):
    def post_request(self, media_type:str, media_id: int, seasons: list[int]=None) -> MediaRequest:
        """POST request on request endpoint"""

        if media_type not in ("movie", "tv", "person"):
            raise JellyseerrException(f"media_type must be one of 'movie', 'tv', 'person'; got '{media_type}'")

        if media_type == "tv" and seasons is None:
            raise JellyseerrException("Must provide seasons to request for media_type 'tv'")

        payload = {
            "mediaType": media_type,
            "mediaId": media_id
        }

        if seasons is not None:
            payload["seasons"] = seasons

        try:
            response = self._post(self.url + "request", json=payload)
            response.raise_for_status()

        except requests.HTTPError as e:
            raise JellyseerrException(e.response.json())

        return MediaRequest.deserialize(response.json())

    def get_request(self):
        """GET requests on request endpoint"""
        response = self._get(self.url + "request")
        return RequestList.deserialize(response.json())

    def delete_request(self, id):
        """DELETE request on request endpoint"""

        try:
            response = self._delete(self.url + f"request/{id}")
            response.raise_for_status()

        except requests.HTTPError as e:
            raise JellyseerrException(e.response.json())
