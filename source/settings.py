"""Define project settings. Will try to import local settings file accessing settings in this file."""
import os
import pathlib

from dotenv import load_dotenv, dotenv_values

env_path = pathlib.Path(__file__).parent.parent / ".local.env"
load_dotenv(env_path)
a = dotenv_values(env_path)


JELLYSEERR_HOSTNAME = os.getenv("JELLYSEERR_HOSTNAME", 'http://jellyseerr:5055')
JELLYSEERR_API = JELLYSEERR_HOSTNAME + "/api/v1/"

JELLYFIN_HOSTNAME = os.getenv("JELLYFIN_HOSTNAME")
JELLYFIN_USERNAME = os.getenv("JELLYFIN_USERNAME")
JELLYFIN_EMAIL = os.getenv("JELLYFIN_EMAIL")
JELLYFIN_PASSWORD = os.getenv("JELLYFIN_PASSWORD")

TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")

TELEGRAM_API_ID = os.getenv("TELEGRAM_API_ID")
TELEGRAM_API_HASH = os.getenv("TELEGRAM_API_HASH")
TELETHON_SESSION_STRING = os.getenv("TELETHON_SESSION_STRING")
