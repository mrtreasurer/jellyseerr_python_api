from bot import create_bot

from settings import TELEGRAM_BOT_TOKEN

bot = create_bot(TELEGRAM_BOT_TOKEN, persist_file="data.pickle")
bot.run_polling()
