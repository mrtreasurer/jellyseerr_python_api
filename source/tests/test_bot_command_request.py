import time

from telegram import InputMedia
from unittest.mock import AsyncMock

from api import JellyseerrAPI
from api.models import Results
from bot import text
from bot.commands import command_request, callback_request
from bot.commands.request import create_keyboard
from settings import JELLYSEERR_API

from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandRequestTestCase1(TelegramUnittestBaseTestCase):
    def test_command_request(self):
        """Test that request command works"""

        update = AsyncMock()
        update.effective_chat.id = 0
        update.effective_message.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["api_key"] = self.get_key()
        context.args = ["gravity"]

        _, update, context = self.run_async_def(command_request, update=update, context=context)

        context.bot.send_photo.assert_called()
        self.assertTrue("requests" in context.user_data)

        key = (update.effective_chat.id, update.effective_message.id)

        self.assertTrue(key in context.user_data["requests"])
        self.assertIsInstance(context.user_data["requests"][key], Results)

    def test_callback_request_cancel(self):
        """Test that callback command works for cancel button"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.callback_query.data = ("cancel",)
        update.callback_query.message.reply_to_message.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        key = (update.effective_chat.id, update.callback_query.message.reply_to_message.id)
        context.user_data["api_key"] = self.get_key()
        context.user_data["requests"] = dict()
        context.user_data["requests"][key] = JellyseerrAPI(JELLYSEERR_API, self.get_key()).search("gravity")

        _, update, context = self.run_async_def(callback_request, update=update, context=context)

        update.callback_query.answer.assert_called()
        update.callback_query.delete_message.assert_called()
        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.callback_query.message.reply_to_message.id,
            text="Search cancelled"
        )

        self.assertTrue("requests" in context.user_data)
        self.assertFalse(key in context.user_data["requests"])

    def test_callback_request_next(self):
        """Test that callback command works for next button"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.callback_query.data = ("next", 3)
        update.callback_query.message.reply_to_message.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        key = (update.effective_chat.id, update.callback_query.message.reply_to_message.id)
        context.user_data["requests"] = dict()
        context.user_data["api_key"] = self.get_key()
        context.user_data["requests"][key] = JellyseerrAPI(JELLYSEERR_API, self.get_key()).search("gravity")

        _, update, context = self.run_async_def(callback_request, update=update, context=context)

        result = context.user_data["requests"][key][3]

        update.callback_query.answer.assert_called()
        update.callback_query.edit_message_media.assert_called()
        update.callback_query.edit_message_media.assert_called_with(
            media=InputMedia("photo", result.poster_path, caption=f"{result}\n\n{result.overview}"),
            reply_markup=create_keyboard(3)
        )

    def test_callback_request_previous(self):
        """Test that callback command works for previous button"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.callback_query.data = ("previous", 1)
        update.callback_query.message.reply_to_message.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        key = (update.effective_chat.id, update.callback_query.message.reply_to_message.id)
        context.user_data["requests"] = dict()
        context.user_data["api_key"] = self.get_key()
        context.user_data["requests"][key] = JellyseerrAPI(JELLYSEERR_API, self.get_key()).search("gravity")

        _, update, context = self.run_async_def(callback_request, update=update, context=context)

        result = context.user_data["requests"][key][1]

        update.callback_query.answer.assert_called()
        update.callback_query.edit_message_media.assert_called()
        update.callback_query.edit_message_media.assert_called_with(
            media=InputMedia("photo", result.poster_path, caption=f"{result}\n\n{result.overview}"),
            reply_markup=create_keyboard(1)
        )

    def test_callback_request_add(self):
        """Test that callback command works for add button"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.callback_query.data = ("add", 0)
        update.callback_query.message.reply_to_message.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        key = (update.effective_chat.id, update.callback_query.message.reply_to_message.id)
        context.user_data["requests"] = dict()
        context.user_data["api_key"] = self.get_key()
        context.user_data["requests"][key] = JellyseerrAPI(JELLYSEERR_API, self.get_key()).search("gravity")

        result = context.user_data["requests"][key][0]

        _, update, context = self.run_async_def(callback_request, update=update, context=context)

        update.callback_query.answer.assert_called()
        update.callback_query.delete_message.assert_called()
        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(
            chat_id=update.effective_chat.id,
            reply_to_message_id=update.callback_query.message.reply_to_message.id,
            text=f"Successfully added {result}"
        )

        self.assertTrue("requests" in context.user_data)
        self.assertFalse(key in context.user_data["requests"])


class BotCommandRequestTestCase2(TelegramIntegrationBaseTestCase):
    def test_request(self):
        """Integration test of request command"""
        # authorize
        self.authorize()
        # send request
        _, responses = self.tester.send_message("/request how to train your dragon", replies=1)

        # check message has keyboard with four buttons
        message_0 = responses[0].message
        self.assertEqual(message_0.button_count, 4)

        # check next button
        self.assertEqual(message_0.buttons[0][1].text, text.request_button_next)
        _, responses = self.tester.click_button(message_0.buttons[0][1], edits=1)
        message_1 = responses[0].message
        self.assertNotEqual(message_0.text, message_1.text)

        # check previous button
        self.assertEqual(message_1.buttons[0][0].text, text.request_button_previous)
        _, responses = self.tester.click_button(message_1.buttons[0][0], edits=1)
        message_2 = responses[0].message
        self.assertNotEqual(message_1.text, message_2.text)
        self.assertEqual(message_0.text, message_2.text)

        # check add button
        self.assertEqual(message_2.buttons[1][0].text, text.request_button_add)
        _, responses = self.tester.click_button(message_2.buttons[1][0], replies=1)
        self.assertTrue(responses[0].text.startswith("Successfully added"))

    def test_request_cancel(self):
        """Integration test for cancel button"""
        # authorize
        self.authorize()
        # send request
        _, responses = self.tester.send_message("/request how to train your dragon", replies=1)

        # check message has keyboard with four buttons
        message_0 = responses[0].message
        self.assertEqual(message_0.button_count, 4)

        # check cancel button
        self.assertEqual(message_0.buttons[1][1].text, text.request_button_cancel)
        _, responses = self.tester.click_button(message_0.buttons[1][1], replies=1)
        self.assertEqual(responses[0].text, text.request_cancel)

    def test_request_next(self):
        """Test next cycling through"""

        # authorize
        self.authorize()
        # send request
        _, responses = self.tester.send_message("/request gravity", replies=1)

        # check message has keyboard with four buttons
        message_0 = responses[0].message
        self.assertEqual(message_0.button_count, 4)

        user_data = self.bot_app.user_data[self.tester.me.id]
        results = list(user_data["requests"].values())[0]
        self.assertGreaterEqual(results.total_results, 20)

        message = message_0
        for i in range(1, 21):
            time.sleep(1)
            button = message.buttons[0][1]
            _, responses = self.tester.click_button(button, edits=1)
            message = responses[0].message

            if i != 20:
                self.assertNotEqual(message_0.text, message.text)

            else:
                self.assertEqual(message_0.text, message.text)

        _, _ = self.tester.click_button(message.buttons[1][1], replies=1)
