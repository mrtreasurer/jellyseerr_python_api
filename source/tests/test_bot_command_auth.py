import settings as s

from telegram.ext import ConversationHandler
from unittest.mock import AsyncMock

from bot import text
from bot.commands.auth import command_auth, command_x, prompt_username, prompt_password_save_username, message_auth_check, USERNAME, PASSWORD
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandAuthTestCase1(TelegramUnittestBaseTestCase):
    def test_command_auth(self):
        """Test that auth command returns correct conversation state and message"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = "/auth"

        context = AsyncMock()
        context.configure_mock(user_data=dict())

        result, update, context = self.run_async_def(command_auth, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_5)

        self.assertTrue("auth_conv" in context.user_data)
        self.assertEqual(len(context.user_data["auth_conv"]), 2)

        self.assertEqual(result, USERNAME)

    def test_command_x(self):
        """Test that x command return correct conversation state and message"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = "/x"

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["username"] = self.faker.user_name()
        context.user_data["auth_conv"] = list()

        result, update, context = self.run_async_def(command_x, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.delete_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_6)

        self.assertFalse("username" in context.user_data)
        self.assertFalse("auth_conv" in context.user_data)
        self.assertEqual(result, ConversationHandler.END)

    def test_prompt_username(self):
        """test that username prompt returns the correct state and message"""
        update = AsyncMock()
        update.effective_chat.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["auth_conv"] = list()

        result, update, context = self.run_async_def(prompt_username, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_5)

        self.assertEqual(len(context.user_data["auth_conv"]), 1)
        self.assertEqual(result, USERNAME)

    def test_prompt_password(self):
        """test that password prompt
        - saves username in userdata
        - prompts password with correct message
        - returns correct conversation state.
        """
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = self.faker.user_name()

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["auth_conv"] = list()

        result, update, context = self.run_async_def(prompt_password_save_username, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_2)

        self.assertTrue("username" in context.user_data)
        self.assertEqual(len(context.user_data["auth_conv"]), 2)
        self.assertEqual(result, PASSWORD)

    def test_auth_check_fail(self):
        """Test that we can sign in successfully or not with correct messages and state"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = self.faker.password()

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["username"] = self.faker.user_name()
        context.user_data["auth_conv"] = list()

        result, update, context = self.run_async_def(message_auth_check, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.delete_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_4)

        self.assertFalse("username" in context.user_data)
        self.assertFalse("auth_conv" in context.user_data)
        self.assertEqual(result, ConversationHandler.END)

    def test_auth_check_success(self):
        """Test that we can sign in successfully or not with correct messages and state"""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = s.JELLYFIN_PASSWORD

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.user_data["username"] = s.JELLYFIN_USERNAME
        context.user_data["auth_conv"] = list()

        result, update, context = self.run_async_def(message_auth_check, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.delete_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_3)

        self.assertFalse("username" in context.user_data)
        self.assertFalse("auth_conv" in context.user_data)
        self.assertTrue("api_key" in context.user_data)
        self.assertEqual(result, ConversationHandler.END)


class BotCommandAuthTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_auth(self):
        """Test command auth all the way."""
        me = self.tester.me

        _, responses = self.tester.send_message("/auth", replies=2)
        self.assertEqual(responses[0].text, text.auth_message_1)
        self.assertEqual(responses[1].text, text.auth_message_5)

        _, responses = self.tester.send_message(s.JELLYFIN_USERNAME, replies=1)
        self.assertEqual(responses[0].text, text.auth_message_2)

        user_data = self.bot_app.user_data[me.id]
        self.assertTrue("username" in user_data)
        self.assertEqual(user_data["username"], s.JELLYFIN_USERNAME)

        _, responses = self.tester.send_message(s.JELLYFIN_PASSWORD, replies=1)
        self.assertEqual(responses[0].text, text.auth_message_3)

        user_data = self.bot_app.user_data[me.id]
        self.assertFalse("username" in user_data)
        self.assertTrue("api_key" in user_data)

    def test_cancel_2(self):
        """Test that we can cancel the /auth command after username"""
        me = self.tester.me

        original_user_data = self.bot_app.user_data[me.id]

        _, responses_1 = self.tester.send_message("/auth", replies=2)
        self.assertEqual(responses_1[0].text, text.auth_message_1)
        self.assertEqual(responses_1[1].text, text.auth_message_5)

        _, responses_2 = self.tester.send_message(s.JELLYFIN_USERNAME, replies=1)
        self.assertEqual(responses_2[0].text, text.auth_message_2)

        _, responses_3 = self.tester.send_message("/x", replies=1)
        self.assertEqual(responses_3[0].text, text.auth_message_6)

        user_data = self.bot_app.user_data[me.id]
        self.assertFalse("username" in user_data)
        self.assertDictEqual(user_data, original_user_data)

        _, responses_3 = self.tester.send_message("/x", replies=1)
        self.assertEqual(responses_3[0].text, text.unknown_message)

    def test_cancel_1(self):
        """Test that we can cancel the /auth command before username"""
        me = self.tester.me

        original_user_data = self.bot_app.user_data[me.id]

        _, responses_1 = self.tester.send_message("/auth", replies=2)
        self.assertEqual(responses_1[0].text, text.auth_message_1)
        self.assertEqual(responses_1[1].text, text.auth_message_5)

        _, responses_3 = self.tester.send_message("/x", replies=1)
        self.assertEqual(responses_3[0].text, text.auth_message_6)

        user_data = self.bot_app.user_data[me.id]
        self.assertFalse("username" in user_data)
        self.assertDictEqual(user_data, original_user_data)
