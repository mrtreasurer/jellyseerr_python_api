import asyncio
import unittest

import settings as s

from faker import Faker
from unittest.mock import AsyncMock

from api import JellyseerrAPI
from bot import create_bot

from tests.telegram_tester import TelegramTester

class BaseTestCase(unittest.TestCase):
    faker = Faker()


class RequestBaseTestCase(BaseTestCase):
    jellyseerr = JellyseerrAPI(s.JELLYSEERR_API)

    @classmethod
    def setUpClass(cls):
        """Log in to jellyseerr to authenticate session for testing."""
        cls.jellyseerr.auth_jellyfin(username=s.JELLYFIN_USERNAME, password=s.JELLYFIN_PASSWORD, hostname=s.JELLYFIN_HOSTNAME)


class TelegramBaseTestCase(BaseTestCase):
    @staticmethod
    def get_key():
        """Get api key from jellyseerr API to use for bot authentication"""
        jellyseerr_api = JellyseerrAPI(s.JELLYSEERR_API)
        jellyseerr_api.auth_jellyfin(username=s.JELLYFIN_USERNAME, password=s.JELLYFIN_PASSWORD, hostname=s.JELLYFIN_HOSTNAME)
        api_key = jellyseerr_api.api_key

        return api_key


class TelegramUnittestBaseTestCase(TelegramBaseTestCase):
    loop = asyncio.get_event_loop()

    def run_async_def(self, func, update=None, context=None):
        """Create AsyncMock objects and tests function. Returns context and update mocks to test."""
        if not update:
            update = AsyncMock()

        if not context:
            context = AsyncMock()

        result = self.loop.run_until_complete(func(update=update, context=context))
        return result, update, context


class TelegramIntegrationBaseTestCase(TelegramBaseTestCase):
    loop = asyncio.get_event_loop()
    tester = TelegramTester(s.TELETHON_SESSION_STRING, s.TELEGRAM_API_ID, s.TELEGRAM_API_HASH, "@pandatijgertestbot")
    bot_app = create_bot(s.TELEGRAM_BOT_TOKEN)

    def authorize(self):
        self.tester.send_message(f"/key {self.get_key()}", replies=1)

    def setUp(self) -> None:
        self.loop.run_until_complete(self.bot_app.initialize())
        self.loop.run_until_complete(self.bot_app.updater.start_polling())
        self.loop.run_until_complete(self.bot_app.start())

    def tearDown(self) -> None:
        self.loop.run_until_complete(self.bot_app.updater.stop())
        self.loop.run_until_complete(self.bot_app.stop())
        self.loop.run_until_complete(self.bot_app.shutdown())
