from telegram import Update
from telegram.ext import ContextTypes
from unittest.mock import AsyncMock

from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase

from bot import text
from bot.auth import authenticated
from bot.commands import command_request


class BotAuthorisationTestCase1(TelegramUnittestBaseTestCase):
    @staticmethod
    async def func(update: Update, context: ContextTypes.DEFAULT_TYPE):
        """dummy method to test authenticated decorator"""
        return 1

    def test_auth(self):
        """Test that the decorator executes the passed function if auth is present"""
        # create update and context mocks
        update = AsyncMock()
        update.effective_chat.id = 0

        context = AsyncMock()
        context.configure_mock(user_data=dict())

        # get decorator function
        decorator = authenticated(self.func)

        # check that no creds return unauthorised message
        _, update, context = self.run_async_def(decorator, update=update, context=context)
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.unauthorised_message)

        # set key
        context.user_data["api_key"] = self.get_key()

        # check that with creds, the function is executed
        self.assertEqual(self.loop.run_until_complete(self.func(update, context)), self.loop.run_until_complete(decorator(update, context)))


class BotAuthorisationTestCase2(TelegramIntegrationBaseTestCase):
    def test_request(self):
        """Test authentication on /request call"""

        # send request without auth
        _, responses = self.tester.send_message("/request how to train your dragon", replies=1)
        self.assertEqual(responses[0].text, text.unauthorised_message)

        # send request with auth
        self.authorize()
        _, responses = self.tester.send_message("/request how to train your dragon", replies=1)
        self.assertEqual(responses[0].button_count, 4)

        _, responses = self.tester.click_button(responses[0].buttons[1][1], replies=1)
        self.assertEqual(responses[0].text, text.request_cancel)
