from api.models.base_model import BaseModel
from api.models import Results
from tests.template import RequestBaseTestCase


class JellyseerrSearchTestCase(RequestBaseTestCase):
    def test_search(self):
        """Test that we can successfully retrieve search results."""

        for query in ["die hard", "how i met your mother", "john cleese", "how to train your dragon"]:
            result = self.jellyseerr.search(query=query)

            self.assertIsInstance(result, Results)
            self.assertGreater(result.total_results, 0)
            for r in result:
                self.assertIsInstance(r, BaseModel)
