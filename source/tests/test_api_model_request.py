from datetime import datetime

from api.models import MediaRequest
from tests.template import BaseTestCase


class JellyseerrRequestModelTestCase(BaseTestCase):
    def test_updated_created(self):
        """Test that the properties updated_at and created_at return datetimes"""
        request = MediaRequest(
            id=self.faker.pyint(),
            type="movie",
            status=None,
            media=None,
            created_at="2011-11-04T00:05:23.000Z",
            updated_at='2023-06-23T11:58:15.000Z',
            requested_by=None,
            modified_by=None,
        )

        self.assertIsInstance(request.creation_date, datetime)
        self.assertEqual(request.creation_date.year, 2011)

        self.assertIsInstance(request.update_date, datetime)
        self.assertEqual(request.update_date.year, 2023)
        # I would like to check the time offset, but I don't know a way that isn't circular
