from unittest.mock import AsyncMock

from bot import text
from bot.commands import command_start
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandStartTestCase1(TelegramUnittestBaseTestCase):
    def test_command_start(self):
        """Test that function returns correct response"""
        update = AsyncMock()
        update.effective_chat.id = 0

        _, update, context = self.run_async_def(command_start, update=update)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.start_message)

class BotCommandStartTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_start(self):
        """Test that sending command returns correct response"""
        _, response = self.tester.send_message("/start", replies=1)

        self.assertEqual(response[0].text, text.start_message)
