from unittest.mock import AsyncMock

from bot import text
from bot.commands import command_url
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandUrlTestCase1(TelegramUnittestBaseTestCase):
    def test_command_url(self):
        """Test that function returns correct response"""
        update = AsyncMock()
        update.effective_chat.id = 0

        _, update, context = self.run_async_def(command_url, update=update)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.url_message)


class BotCommandUrlTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_url(self):
        """Test that sending command returns correct response"""
        _, response = self.tester.send_message("/url", replies=1)

        self.assertEqual(response[0].text, text.url_message)
