from api import JellyseerrException
from api.models import MediaRequest, RequestList
from tests.template import RequestBaseTestCase


class JellyseerrRequestTestCase(RequestBaseTestCase):
    def tearDown(self) -> None:
        """Delete all requests from server"""
        request_list = self.jellyseerr.get_request()
        for request in request_list:
            self.jellyseerr.delete_request(request.id)

        self.assertEqual(self.jellyseerr.get_request().page_info.results, 0)

    def test_get_requests(self):
        """Test that we can get all current requests"""
        request_1 = self.jellyseerr.post_request("movie", 157336)
        self.assertIsInstance(request_1, MediaRequest)

        request_2 = self.jellyseerr.post_request("tv", 4614, [1])
        self.assertIsInstance(request_2, MediaRequest)

        request_list = self.jellyseerr.get_request()
        self.assertIsInstance(request_list, RequestList)
        self.assertEqual(request_list.page_info.results, 2)

    def test_delete_request(self):
        """Test that we can delete a request"""
        request_1 = self.jellyseerr.post_request("movie", 157336)
        self.assertIsInstance(request_1, MediaRequest)

        request_2 = self.jellyseerr.post_request("tv", 4614, [1])
        self.assertIsInstance(request_2, MediaRequest)

        request_list_1 = self.jellyseerr.get_request()
        self.assertIsInstance(request_list_1, RequestList)
        self.assertEqual(request_list_1.page_info.results, 2)

        self.jellyseerr.delete_request(request_1.id)
        self.jellyseerr.delete_request(request_2.id)

        request_list_2 = self.jellyseerr.get_request()
        self.assertIsInstance(request_list_2, RequestList)
        self.assertEqual(request_list_2.page_info.results, 0)

    def test_request_invalid_type(self):
        """Test that if we request invalid media_type we get expected exception thrown"""
        with self.assertRaises(JellyseerrException) as e:
            _ = self.jellyseerr.post_request("foo", 1234)

        self.assertEqual(e.exception.args[0], "media_type must be one of 'movie', 'tv', 'person'; got 'foo'")

    def test_request_movie(self):
        """Test that we can request a movie"""

        # id is for interstellar (2014) movie
        request = self.jellyseerr.post_request("movie", 157336)
        self.assertIsInstance(request, MediaRequest)
        self.assertEqual(request.media.tmdb_id, 157336)

    def test_request_tv_show(self):
        """Test that we can request a tv show"""

        # id is for NCIS (2003) series
        request = self.jellyseerr.post_request("tv", 4614, [1])
        self.assertIsInstance(request, MediaRequest)
        self.assertEqual(request.media.tmdb_id, 4614)

        # assert exception is raised when no seasons provided
        with self.assertRaises(JellyseerrException) as e:
            _ = self.jellyseerr.post_request("tv", 4614)

        self.assertEqual(e.exception.args[0], "Must provide seasons to request for media_type 'tv'")
