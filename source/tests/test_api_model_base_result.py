from api.models import BaseResult

from tests.template import BaseTestCase


class JellyseerrBaseResultModelTestCase(BaseTestCase):
    def test_date(self):
        """Test that the media_date attribute points to the correct attribute."""
        media = BaseResult()
        media.DATE_FIELD = "foo"
        media.foo = "2017-11-04"
        self.assertEqual(media.media_date, media.foo)

    def test_name(self):
        """Test that the media_name attribute points to the correct attribute."""
        media = BaseResult()
        media.NAME_FIELD = "foo"
        media.foo = "Doofenschmirtz"
        self.assertEqual(media.media_name, media.foo)

    def test_year(self):
        """Test that the year property gives the year or None if the DATE attribute is invalid"""
        media = BaseResult()
        media.date = "2004-04-15"
        self.assertEqual(media.year, 2004)

        media = BaseResult()
        media.date = "2004-14-15"
        self.assertEqual(media.year, None)

        media = BaseResult()
        media.date = ""
        self.assertEqual(media.year, None)

    def test_poster_path(self):
        """Test that the poster path is generated correctly"""
        media = BaseResult()
        media.poster_path = f"/{self.faker.pystr(20)}.jpg"
        self.assertTrue(media.get_poster_path().startswith("https://image.tmdb.org/t/p/w500"))

        media = BaseResult()
        media.poster_path = None
        self.assertEqual(media.get_poster_path(), media.missing_poster)

    def test_string(self):
        """Test the string representation of Media"""
        media = BaseResult()
        media.name = "Doofenschmirtz"
        media.date = "1999-12-07"

        self.assertEqual(str(media), f"{media.media_name} ({media.year})")
