import settings as s

from api import JellyseerrAPI
from api.exception import JellyseerrException
from api.models import User
from tests.template import RequestBaseTestCase


class JellyseerrAuthTestCase(RequestBaseTestCase):

    @classmethod
    def setUpClass(cls):
        pass

    def test_auth_0_fail_no_hostname(self):
        """Assert that login fails with the correct exception when no hostname is provided."""
        with self.assertRaises(JellyseerrException) as e:
            _ = self.jellyseerr.auth_jellyfin(username=s.JELLYFIN_USERNAME, password=s.JELLYFIN_PASSWORD)

        self.assertEqual(e.exception.args[0], {'error': 'No hostname provided.'})

    def test_auth_1_success(self):
        """Assert that login is successfull with hostname and email."""
        user = self.jellyseerr.auth_jellyfin(username=s.JELLYFIN_USERNAME, password=s.JELLYFIN_PASSWORD, email=s.JELLYFIN_EMAIL, hostname=s.JELLYFIN_HOSTNAME)
        self.assertIsInstance(user, User)
        self.assertEqual(user.jellyfin_username, s.JELLYFIN_USERNAME)
        self.assertIsNotNone(self.jellyseerr.api_key)

    def test_auth_fail_wrong_credentials(self):
        """Assert that wrong username/password raise error"""

        with self.assertRaises(JellyseerrException) as e:
            _ = self.jellyseerr.auth_jellyfin(username=self.faker.user_name(), password=self.faker.password())

        self.assertEqual(e.exception.args[0], {"message": "Unauthorized"})

    def test_auth_email_as_username(self):
        """Assert that email address can be used in the username field"""
        user = self.jellyseerr.auth_jellyfin(username=s.JELLYFIN_EMAIL, password=s.JELLYFIN_PASSWORD)
        self.assertIsInstance(user, User)
        self.assertEqual(user.jellyfin_username, s.JELLYFIN_USERNAME)

    def test_auth_apikey(self):
        """Assert that we can authenticate with api key."""
        # obtain an api key
        self.jellyseerr.auth_jellyfin(username=s.JELLYFIN_USERNAME, password=s.JELLYFIN_PASSWORD, email=s.JELLYFIN_EMAIL, hostname=s.JELLYFIN_HOSTNAME)
        api_key = self.jellyseerr.api_key

        # create new instance with key. make sure key is set.
        jellyseer_api = JellyseerrAPI(s.JELLYSEERR_API, api_key)
        self.assertEqual(jellyseer_api.api_key, api_key)

        # check that the correct user is returned
        user = jellyseer_api.auth_me()
        self.assertIsInstance(user, User)
        self.assertEqual(user.jellyfin_username, s.JELLYFIN_USERNAME)

    def test_auth_me_no_credentials(self):
        """Assert that calling auth/me with no credentials raises correct exception"""
        jellyseerr_api = JellyseerrAPI(s.JELLYSEERR_API)
        with self.assertRaises(JellyseerrException) as e:
            _ = jellyseerr_api.auth_me()

        content = e.exception.args[0]
        self.assertEqual(content["message"], "cookie 'connect.sid' required")

    def test_wrong_key(self):
        """Assert that setting an invalid key raises an exception and doesn't change the key."""
        jellyseerr_api = JellyseerrAPI(s.JELLYSEERR_API)
        self.assertIsNone(jellyseerr_api.api_key)
        with self.assertRaises(JellyseerrException) as e:
            _ = jellyseerr_api.api_key = self.faker.password()

        content = e.exception.args[0]
        self.assertEqual(content["error"], "You do not have permission to access this endpoint")
        self.assertIsNone(jellyseerr_api.api_key)
