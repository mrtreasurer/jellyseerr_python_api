import os

from tests.template import TelegramIntegrationBaseTestCase

from bot import create_bot, text

from settings import TELEGRAM_BOT_TOKEN


class BotPersistenceTestCase(TelegramIntegrationBaseTestCase):
    persist_file = "test.pickle"
    bot_app = create_bot(TELEGRAM_BOT_TOKEN, persist_file=persist_file)

    def test_persistence_0(self):
        """Authenticate in this test. Make sure key is in user_data"""
        api_key = self.get_key()
        me = self.tester.me

        _, responses = self.tester.send_message(f"/key {api_key}", replies=1)
        self.assertEqual(responses[0].text, text.auth_message_3)

        self.assertTrue(me.id in self.bot_app.user_data)
        user_data = self.bot_app.user_data[me.id]

        self.assertTrue("api_key" in user_data)
        self.assertEqual(user_data["api_key"], api_key)

        self.loop.run_until_complete(self.bot_app.persistence.flush())

    def test_persistence_1(self):
        """Assert that api_key is still in user data"""
        api_key = self.get_key()
        me = self.tester.me

        self.assertTrue(me.id in self.bot_app.user_data)
        user_data = self.bot_app.user_data[me.id]

        self.assertTrue("api_key" in user_data)
        self.assertEqual(user_data["api_key"], api_key)

    @classmethod
    def tearDownClass(cls) -> None:
        os.remove(cls.bot_app.persistence.filepath)
        return super().tearDownClass()
