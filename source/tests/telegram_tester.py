import asyncio
import logging

from telethon import TelegramClient, events
from telethon.sessions import StringSession


logging.basicConfig(level=logging.DEBUG)
logging.getLogger('telethon').setLevel(logging.CRITICAL)
logging.getLogger('telegram').setLevel(logging.CRITICAL)


class TelegramTester:
    def __init__(self, session_string, api_id, api_hash, bot_username) -> None:
        """This class uses telethon.TelegramClient to send message to a bot and saves its responses.

        :param session_string: Session string for telethon client.
        :type session_string: str.
        :param api_id: Telegram API id.
        :type api_id: int.
        :param api_hash: Telegram API hash.
        :type api_hash: str.
        :param bot_username: Username of bot to send messages to. Doesn't have to be a bot btw.
        :type bot_username: str.
        """
        self.client = TelegramClient(StringSession(session_string), api_id, api_hash)
        self.messages = list()
        self.bot_username = bot_username
        self.new_message_filter = events.NewMessage(from_users=bot_username)
        self.edited_message_filter = events.MessageEdited(from_users=bot_username)
        self.loop = asyncio.get_event_loop()

    @property
    def me(self):
        async def main():
            return await self.client.get_me()

        self.remove_handlers()
        return self.run(main)

    def message_handler(self, n_messages):
        """Create message handler function that disconnects the client after a number of responses is received.

        :param n_messages: Number of messages to receive before disconnecting.
        :type n_messages: int.
        """
        async def _message_handler(event):
            await event.message.mark_read()
            self.messages.append(event)

            if len(self.messages) >= n_messages:
                await self.client.disconnect()

        return _message_handler

    def run(self, func, *args, disconnect=True, timeout=5, **kwargs):
        """Run an async def and return the result. Telethon client is started and disconnect is awaited.

        :param func: async func to run.
        :type func: coroutine.
        """
        async def main():
            await self.client.connect()
            result = await func(*args, **kwargs)

            if disconnect:
                await self.client.disconnect()

            await self.client.disconnected
            return result

        return self.loop.run_until_complete(asyncio.wait_for(main(), timeout))

    def remove_handlers(self):
        """Remove all event handlers from client."""
        for handler, _ in self.client.list_event_handlers():
            self.client.remove_event_handler(handler)

        return self.client

    def set_message_handler(self, replies=None, edits=None):
        """Configure client with message handler that waits for a specified number of messages and then disconnects.

        :param replies: number of messages to wait for, defaults to None. Mutually exclisive with edits.
        :type replies: int, optional
        :param edits: number of edited messages to wait for, defaults to None. Mutually exclusive with replies.
        :type edits: int, optional
        :return: Telethon client.
        :rtype: telethon.TelegramClient
        """

        assert (replies is None) or (edits is None), "cannot set both number of replies and edits"
        assert (replies is not None) or (edits is not None), "must set either number of replies or edits"

        self.remove_handlers()
        if replies is not None:
            self.client.add_event_handler(self.message_handler(replies), self.new_message_filter)

        if edits is not None:
            self.client.add_event_handler(self.message_handler(edits), self.edited_message_filter)

        return self.client

    def send_message(self, text, replies=None, **kwargs):
        """Send message to bot then wait for the client to disconnect.

        :param text: Text message to be sent.
        :type text: str.
        :param replies: Number of replies before disconnect.
        :type replies: int.
        :return: Sent message object and list of replies.
        :rtype: tuple.
        """
        self.set_message_handler(replies=replies, **kwargs)
        self.messages.clear()

        async def func():
            return await self.client.send_message(self.bot_username, text)

        message = self.run(func, disconnect=False)

        return message, self.messages

    def click_button(self, button, replies=None, edits=None, **kwargs):
        """Click button and wait for action.

        :param button: _description_
        :type button: _type_
        :param replies: _description_, defaults to None
        :type replies: _type_, optional
        :param edits: _description_, defaults to None
        :type edits: _type_, optional
        """

        self.set_message_handler(replies=replies, edits=edits, **kwargs)
        self.messages.clear()

        async def func():
            return await button.click()

        message = self.run(func, disconnect=False)

        return message, self.messages
