from telethon import TelegramClient
from telethon.sessions import StringSession

# get these from my.telegram.org under api development tools
api_id = None
api_hash = None

# create client
client = TelegramClient("test", api_id, api_hash)

# run a random function
async def main():
    await client.get_me()

with client:
    client.loop.run_until_complete(main())

# get session string
print(StringSession.save(client.session))
