from unittest.mock import AsyncMock

from bot import text
from bot.commands import command_help
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandHelpTestCase1(TelegramUnittestBaseTestCase):
    def test_command_help(self):
        """Test that function returns correct response"""
        update = AsyncMock()
        update.effective_chat.id = 0

        _, update, context = self.run_async_def(command_help, update=update)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.help_message)

class BotCommandHelpTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_help(self):
        """Test that sending command returns correct response"""
        _, response = self.tester.send_message("/help", replies=1)

        self.assertEqual(response[0].text, text.help_message)
