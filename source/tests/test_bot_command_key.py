import settings as s

from unittest.mock import AsyncMock

from bot import text
from bot.commands import command_key
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandKeyTestCase1(TelegramUnittestBaseTestCase):
    def test_command_key_success(self):
        """Test that sending the correct key returns the success message."""
        api_key = self.get_key()
        self.assertIsNotNone(api_key)

        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = f"/key {api_key}"

        context = AsyncMock()
        context.configure_mock(user_data=dict())
        context.args = [api_key]

        _, update, context = self.run_async_def(command_key, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.auth_message_3)

        self.assertTrue("api_key" in context.user_data)
        self.assertEqual(context.user_data["api_key"], api_key)

    def test_command_key_fail(self):
        """Test that sending an invalid key returns the fail message."""
        key = self.faker.password()

        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = f"/key {key}"

        context = AsyncMock()
        context.args = [key]

        _, update, context = self.run_async_def(command_key, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.key_message_fail)

        self.assertFalse("api_key" in context.user_data)

    def test_command_key_nokey(self):
        """Test that sending no key gives error message."""
        update = AsyncMock()
        update.effective_chat.id = 0
        update.message.text = f"/key"

        context = AsyncMock()
        context.args = []

        _, update, context = self.run_async_def(command_key, update=update, context=context)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.key_message_nokey)

        self.assertFalse("api_key" in context.user_data)


class BotCommandKeyTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_key_success(self):
        """Integration: test that sending a correct key returns success message."""
        api_key = self.get_key()
        self.assertIsNotNone(api_key)

        message = f"/key {api_key}"
        _, response = self.tester.send_message(message, replies=1)

        self.assertEqual(response[0].text, text.auth_message_3)

    def test_command_key_fail(self):
        """Integration: test that sending an incorrect key returns error message."""

        message = f"/key {self.faker.password()}"
        _, response = self.tester.send_message(message, replies=1)

        self.assertEqual(response[0].text, text.key_message_fail)

    def test_command_key_nokey(self):
        """Integration: test that sending no key returns error message."""

        message = f"/key"
        _, response = self.tester.send_message(message, replies=1)

        self.assertEqual(response[0].text, text.key_message_nokey)
