from unittest.mock import AsyncMock

from bot import text
from bot.commands import command_unknown
from tests.template import TelegramUnittestBaseTestCase, TelegramIntegrationBaseTestCase


class BotCommandUnknownTestCase1(TelegramUnittestBaseTestCase):
    def test_command_unknown(self):
        """Test that function returns correct response"""
        update = AsyncMock()
        update.effective_chat.id = 0

        _, update, context = self.run_async_def(command_unknown, update=update)

        context.bot.send_message.assert_called()
        context.bot.send_message.assert_called_with(chat_id=update.effective_chat.id, text=text.unknown_message)


class BotCommandUnknownTestCase2(TelegramIntegrationBaseTestCase):
    def test_command_unknown(self):
        """Test that sending unknown command returns correct response"""
        _, response = self.tester.send_message("/somecommand", replies=1)

        self.assertEqual(response[0].text, text.unknown_message)

    def test_message_unknown(self):
        """Test that sending unknown message returns correct response"""
        _, response = self.tester.send_message("foo bar", replies=1)

        self.assertEqual(response[0].text, text.unknown_message)
