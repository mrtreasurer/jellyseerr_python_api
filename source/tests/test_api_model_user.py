from api.models import User
from tests.template import BaseTestCase


class JellyseerrUserModelTestCase(BaseTestCase):
    def test_create_user(self):
        """Test that user can be created from dict"""
        data = {
            "id": self.faker.pyint(),
            "email": self.faker.email(),
            "username": None,
            "plexUsername": None,
            "plexToken": None,
            "jellyfinUsername": self.faker.user_name(),
            "jellyfinAuthToken": self.faker.password(),
            "userType": self.faker.pyint(),
            "permissions": self.faker.pyint(),
            "avatar": None,
            "createdAt": self.faker.date_time(),
            "updatedAt": self.faker.date_time(),
            "requestCount": self.faker.pyint()
        }

        user = User.deserialize(data)
        self.assertIsNotNone(user.id)
