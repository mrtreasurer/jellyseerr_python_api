import settings as s


start_message = f"""Hello, I am a bot. You can use me to request media on the Jellyfin server {s.JELLYFIN_HOSTNAME}. I work through Jellyseerr, which you can access at {s.JELLYSEERR_HOSTNAME} if you want to see your requests. You'll need to authenticate yourself using /auth before you can make requests."""

unknown_message = "Sorry, I didn't understand that. Type /help for a list of available commands."

url_message = s.JELLYSEERR_HOSTNAME

help_message = "\n".join([
    "/auth sign in with username and password to use this bot.",
    "/key {your key} sign in using your Jellyseerr API key to use this bot.",
    "/url get Jellyseerr web address."
])

auth_message_1 = "Let's sign you in to Jellyseerr. For that, I need your login for Jellyfin."
auth_message_5 = "Please send your username or type /x to cancel."
auth_message_2 = "Please send your password or type /x to cancel."
auth_message_3 = "You're authenticated and all set to start requesting media through me. You can use the /request command."
auth_message_4 = "I'm sorry, those credentials didn't work. Check your username and password or ask the server admin for help."
auth_message_6 = "Login cancelled."

key_message_fail = "Oops, that didn't work. This key is not valid."
key_message_nokey = "You didn't provide an API key."

request_button_previous = "Previous"
request_button_next = "Next"
request_button_add = "Add media"
request_button_cancel = "Cancel search"

request_cancel = "Search cancelled"
request_add = "Successfully added {result}"
request_add_fail = "Something went wrong. Try again or contact the maintainer."

unauthorised_message = "You aren't signed in or your credentials are invalid. Use /auth or /key to sign in before using this command."
