import asyncio
import functools

from telegram import Update
from telegram.ext import ContextTypes

from api import JellyseerrAPI, JellyseerrException

from ..text import unauthorised_message


async def message_unauthorised(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Send message saying user is unauthorised"""
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=unauthorised_message
        )


def authenticated(func):
    """Decorator to check if user is authenticated before executing method. If no auth_key is present or the key is invalid, the user gets a message and the method is not executed"""
    @functools.wraps(func)
    async def check_auth(update: Update, context: ContextTypes.DEFAULT_TYPE):
        try:
            JellyseerrAPI(context.user_data["api_key"])

        except (JellyseerrException, KeyError):
            return await message_unauthorised(update, context)

        else:
            return await func(update, context)

    return check_auth
