from telegram import Update
from telegram.ext import ContextTypes, CommandHandler

from ..text import url_message


async def command_url(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=url_message
        )


url_handler = CommandHandler('url', command_url)
