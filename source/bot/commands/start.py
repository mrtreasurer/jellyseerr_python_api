from telegram import Update
from telegram.ext import ContextTypes, CommandHandler

from ..text import start_message


async def command_start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=start_message
        )


start_handler = CommandHandler('start', command_start)
