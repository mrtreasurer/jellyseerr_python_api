from telegram import Update
from telegram.ext import ContextTypes, MessageHandler, filters

from ..text import unknown_message


async def command_unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=unknown_message
        )

unknown_handler = MessageHandler(filters.ALL, command_unknown)
