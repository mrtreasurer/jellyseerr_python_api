from telegram import Update
from telegram.ext import ContextTypes, CommandHandler

from api import JellyseerrAPI, JellyseerrException
from settings import JELLYSEERR_API

from ..text import key_message_fail, auth_message_3, key_message_nokey


async def command_key(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        api_key = context.args[0]

    except IndexError:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=key_message_nokey
            )

        return

    try:
        jellyseerr = JellyseerrAPI(JELLYSEERR_API, api_key)

    except JellyseerrException:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=key_message_fail
            )

    else:
        context.user_data["api_key"] = jellyseerr.api_key

        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=auth_message_3
            )


key_handler = CommandHandler('key', command_key)
