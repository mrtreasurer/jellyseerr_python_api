from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto, CallbackQuery
from telegram.ext import ContextTypes, CommandHandler, CallbackQueryHandler
from telegram.error import BadRequest

from api import JellyseerrAPI, JellyseerrException
from api.models import Results, BaseResult
from settings import JELLYSEERR_API

from ..text import request_button_previous, request_button_add, request_button_cancel, request_button_next, request_add, request_cancel, request_add_fail
from ..auth import authenticated


def create_keyboard(i_result: int) -> InlineKeyboardMarkup:
    return InlineKeyboardMarkup([
        [InlineKeyboardButton(request_button_previous, callback_data=("previous", i_result - 1)), InlineKeyboardButton(request_button_next, callback_data=("next", i_result + 1))],
        [InlineKeyboardButton(request_button_add, callback_data=("add", i_result)), InlineKeyboardButton(request_button_cancel, callback_data=("cancel",))]
    ])


@authenticated
async def command_request(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Act on request command. Return first search result with keyboard to navigate results, add content or cancel search."""

    # gather search results
    results = JellyseerrAPI(JELLYSEERR_API, context.user_data["api_key"]).search(" ".join(context.args))

    # add results to user_data
    if "requests" not in context.user_data:
        context.user_data["requests"]: dict[tuple[int], Results] = dict()
    context.user_data["requests"][(update.effective_chat.id, update.effective_message.id)] = results

    i_result = 0
    result = results[i_result]

    # send message
    await context.bot.send_photo(
        chat_id=update.effective_chat.id,
        reply_to_message_id=update.effective_message.id,
        photo=result.get_poster_path(),
        caption=str(result),
        reply_markup=create_keyboard(i_result)
        )


async def update_message(i: int, res: BaseResult, query: CallbackQuery):
    keyboard = create_keyboard(i)

    try:
        await query.edit_message_media(
            media=InputMediaPhoto(
                res.get_poster_path(),
                caption=str(res)
            ),
            reply_markup=keyboard
        )

    except BadRequest:
        """sometimes the poster gives issues"""
        await query.edit_message_media(
            media=InputMediaPhoto(
                res.missing_poster,
                caption=str(res)
            ),
            reply_markup=keyboard
        )


@authenticated
async def callback_request(update: Update, context: ContextTypes.DEFAULT_TYPE):
    query = update.callback_query
    request_key = (update.effective_chat.id, query.message.reply_to_message.id)

    await query.answer()

    if query.data[0] == "cancel":
        del context.user_data["requests"][request_key]

        await query.delete_message()

        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=query.message.reply_to_message.id,
            text=request_cancel
        )

        return

    command, i_result = query.data
    results = context.user_data["requests"][request_key]

    if command == "next":
        try:
            result = results[i_result]

        except IndexError:
            i_result = 0
            result = results[i_result]

        await update_message(i_result, result, query)

    elif command == "previous":
        result = results[i_result]
        await update_message(i_result, result, query)

    elif command == "add":
        result = results[i_result]
        del context.user_data["requests"][request_key]

        try:
            # todo: only works for movies so far
            JellyseerrAPI(JELLYSEERR_API, context.user_data["api_key"]).post_request(result.media_type, result.id)

        except JellyseerrException:
            await query.delete_message()
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=query.message.reply_to_message.id,
                text=request_add_fail
            )

        else:
            await query.delete_message()
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                reply_to_message_id=query.message.reply_to_message.id,
                text=request_add.format(result=result),
            )


command_request_handler = CommandHandler('request', command_request)
callback_request_handler = CallbackQueryHandler(callback_request)
