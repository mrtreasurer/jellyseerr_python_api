from telegram import Update
from telegram.ext import ContextTypes, CommandHandler

from ..text import help_message


async def command_help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=help_message
        )


help_handler = CommandHandler('help', command_help)
