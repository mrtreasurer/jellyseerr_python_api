from telegram import Update
from telegram.ext import ContextTypes, CommandHandler, ConversationHandler, MessageHandler, filters

from api import JellyseerrAPI, JellyseerrException
from settings import JELLYSEERR_API, JELLYFIN_HOSTNAME

from ..text import auth_message_1, auth_message_2, auth_message_3, auth_message_4, auth_message_5, auth_message_6


USERNAME, PASSWORD = range(2)


async def command_auth(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Start auth conversation. Called on /auth command. Asks for username."""
    message = await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=auth_message_1
        )

    context.user_data["auth_conv"] = [message.id]

    return await prompt_username(update, context)


async def prompt_username(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Ask for username"""
    message = await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=auth_message_5
        )

    context.user_data["auth_conv"].append(message.id)

    return USERNAME


async def prompt_password_save_username(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Ask for password and save username in user_data"""
    context.user_data["username"] = update.message.text
    context.user_data["auth_conv"].append(update.message.id)

    message = await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=auth_message_2
    )

    context.user_data["auth_conv"].append(message.id)

    return PASSWORD


async def clean(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Delete user_data and auth_conversation messages"""

    try:
        del context.user_data["username"]

    except KeyError:
        pass

    for message_id in context.user_data["auth_conv"]:
        await context.bot.delete_message(update.effective_chat.id, message_id)

    del context.user_data["auth_conv"]


async def message_auth_check(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Try to log in to Jellyseerr."""

    context.user_data["auth_conv"].append(update.message.id)

    jellyseerr = JellyseerrAPI(JELLYSEERR_API)

    try:
        _ = jellyseerr.auth_jellyfin(
                username=context.user_data["username"],
                password=update.message.text,
                hostname=JELLYFIN_HOSTNAME
            )

    except JellyseerrException:
        text = auth_message_4

    else:
        context.user_data["api_key"] = jellyseerr.api_key
        text = auth_message_3

    finally:
        await clean(update, context)

        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=text
        )

    return ConversationHandler.END


async def command_x(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """End conversation with /x command"""
    context.user_data["auth_conv"].append(update.message.id)

    await clean(update, context)

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=auth_message_6
    )

    return ConversationHandler.END


auth_handler = ConversationHandler(
    entry_points=[CommandHandler('auth', command_auth)],
    states={
        USERNAME: [MessageHandler(filters.TEXT & ~filters.COMMAND, prompt_password_save_username)],
        PASSWORD: [MessageHandler(filters.TEXT & ~filters.COMMAND, message_auth_check)]
    },
    fallbacks=[CommandHandler("x", command_x)]
    )
