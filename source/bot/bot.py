import logging
import pathlib

from telegram.ext import Application, PicklePersistence

from . import commands as c


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

def create_bot(token, persist_file=None):
    builder = Application.builder()
    builder.token(token)
    builder.arbitrary_callback_data(True)

    if persist_file is not None:
        dir_path = pathlib.Path(__file__).parent.parent.parent / "persistence"
        if not dir_path.exists():
            dir_path.mkdir()
        file_path = dir_path / persist_file

        builder.persistence(PicklePersistence(file_path))

    application = builder.build()

    application.add_handler(c.start_handler)
    application.add_handler(c.help_handler)
    application.add_handler(c.url_handler)
    application.add_handler(c.auth_handler)
    application.add_handler(c.key_handler)
    application.add_handler(c.command_request_handler)
    application.add_handler(c.callback_request_handler)
    # unknown_handler must be added last
    application.add_handler(c.unknown_handler)

    return application
